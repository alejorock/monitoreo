package com.bringsolutions.monitoreo;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.animation.Animator;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.view.ViewPropertyAnimator;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bringsolutions.monitoreo.actores.Gerente;
import com.bringsolutions.monitoreo.actores.JefePlanta;
import com.bringsolutions.monitoreo.actores.Operador;
import com.bringsolutions.monitoreo.actores.Reparador;
import com.bringsolutions.monitoreo.objetos.Constantes;

import es.dmoral.toasty.Toasty;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class Login extends AppCompatActivity {
    private ImageView bookIconImageView;
    private TextView bookITextView;
    private ProgressBar loadingProgressBar;
    private RelativeLayout rootView, afterAnimationView;

    EditText cajaUsuario, cajaPassword;
    Button btnIniciarSesion;
    TextView tvOlvidePassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_login);
        getSupportActionBar().setTitle("                        Inicie Sesión");

        initViews();

        new CountDownTimer(5000, 1000) {

            @Override
            public void onTick(long millisUntilFinished) {
                bookITextView.setVisibility(GONE);
                loadingProgressBar.setVisibility(GONE);
                rootView.setBackgroundColor(ContextCompat.getColor(Login.this, R.color.colorSplashText));
                bookIconImageView.setImageResource(R.drawable.logo_sas);
                startAnimation();
            }

            @Override
            public void onFinish() {

            }
        }.start();


        btnIniciarSesion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String usuario = cajaUsuario.getText().toString(), pass = cajaPassword.getText().toString();
                Intent i;

                if (usuario.isEmpty() || pass.isEmpty()) {
                    Toasty.warning(getApplicationContext(), "Algún campo vacío", Toast.LENGTH_SHORT, true).show();
                } else {
                    if (usuario.equals(Constantes.user_opeador_uno.getUsuario()) && pass.equals(Constantes.user_opeador_uno.getPassword())) {
                        Toasty.success(getApplicationContext(), "¡Bienvenido " + Constantes.user_opeador_uno.getNombre() + "!", Toast.LENGTH_SHORT, true).show();
                        i = new Intent(Login.this, Operador.class);
                        i.putExtra("usuario", Constantes.user_opeador_uno);
                        startActivity(i);
                        // finish();
                    } else if (usuario.equals(Constantes.user_operador_dos.getUsuario()) && pass.equals(Constantes.user_operador_dos.getPassword())) {
                        Toasty.success(getApplicationContext(), "¡Bienvenido " + Constantes.user_operador_dos.getNombre() + "!", Toast.LENGTH_SHORT, true).show();
                        i = new Intent(Login.this, Operador.class);
                        i.putExtra("usuario", Constantes.user_operador_dos);
                        startActivity(i);
                        // finish();
                    } else if (usuario.equals(Constantes.user_operador_tres.getUsuario()) && pass.equals(Constantes.user_operador_tres.getPassword())) {
                        Toasty.success(getApplicationContext(), "¡Bienvenido " + Constantes.user_operador_tres.getNombre() + "!", Toast.LENGTH_SHORT, true).show();
                        i = new Intent(Login.this, Operador.class);
                        i.putExtra("usuario", Constantes.user_operador_tres);
                        startActivity(i);                      //  finish();
                    } else if (usuario.equals(Constantes.user_jefe_planta.getUsuario()) && pass.equals(Constantes.user_jefe_planta.getPassword())) {
                        Toasty.success(getApplicationContext(), "¡Bienvenido " + Constantes.user_jefe_planta.getNombre() + "!", Toast.LENGTH_SHORT, true).show();
                        i = new Intent(Login.this, JefePlanta.class);
                        i.putExtra("usuario", Constantes.user_jefe_planta);
                        startActivity(i);
                        // finish();
                    } else if (usuario.equals(Constantes.user_gerente.getUsuario()) && pass.equals(Constantes.user_gerente.getPassword())) {
                        Toasty.success(getApplicationContext(), "¡Bienvenido " + Constantes.user_gerente.getNombre() + "!", Toast.LENGTH_SHORT, true).show();
                        i = new Intent(Login.this, Gerente.class);
                        i.putExtra("usuario", Constantes.user_gerente);
                        startActivity(i);
                        // finish();
                    } else {
                        Toasty.error(getApplicationContext(), "Algún dato incorrecto o cuenta inexistente", Toast.LENGTH_SHORT, true).show();
                    }
                }
            }
        });

    }

    private void initViews() {
        bookIconImageView = findViewById(R.id.bookIconImageView);
        bookITextView = findViewById(R.id.bookITextView);
        loadingProgressBar = findViewById(R.id.loadingProgressBar);
        rootView = findViewById(R.id.rootView);
        afterAnimationView = findViewById(R.id.afterAnimationView);
        cajaUsuario = findViewById(R.id.cajaUsuario);
        cajaPassword = findViewById(R.id.cajaPassword);
        tvOlvidePassword = findViewById(R.id.tvOlvidePass);
        btnIniciarSesion = findViewById(R.id.loginButton);
    }

    private void startAnimation() {
        ViewPropertyAnimator viewPropertyAnimator = bookIconImageView.animate();
        viewPropertyAnimator.x(50f);
        viewPropertyAnimator.y(100f);
        viewPropertyAnimator.setDuration(1000);
        viewPropertyAnimator.setListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                afterAnimationView.setVisibility(VISIBLE);
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
    }
}
