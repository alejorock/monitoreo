package com.bringsolutions.monitoreo.operaciones;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.bringsolutions.monitoreo.R;
import com.bringsolutions.monitoreo.objetos.Usuario;
import com.github.gcacace.signaturepad.views.SignaturePad;

import java.util.Calendar;

import es.dmoral.toasty.Toasty;

public class ReportarFalla extends AppCompatActivity {
    private TextView tvNombreOperador;
    private Button btnGuardarFirmaREPORTO, btnBorrarFirmaREPORTO, btnGuardarFirmaAUTORIZO, btnBorrarFirmaAUTORIZO, btnCambiarFecha, btnCambiarHora, btnEnviarReporte;
    private SignaturePad cuadroFirmaREPORTO, cuadrofirmaAUTORIZO;
    private EditText cajaFecha, cajaHora;


    private Intent i;
    private Usuario usuario;

    private Calendar calendario = Calendar.getInstance();
    int hora = calendario.get(Calendar.HOUR_OF_DAY);
    int minuto = calendario.get(Calendar.MINUTE);

    //Variables para obtener la fecha
    final int mes = calendario.get(Calendar.MONTH)+1;
    final int dia = calendario.get(Calendar.DAY_OF_MONTH);
    final int anio = calendario.get(Calendar.YEAR);
    private static final String CERO = "0";
    private static final String BARRA = "/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reportar_falla);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        i = getIntent();
        usuario = (Usuario) i.getSerializableExtra("usuario");
        initElements();
        clicks();

    }

    private void clicks() {
        btnGuardarFirmaREPORTO.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toasty.success(getApplicationContext(), "Firma añadida!", Toast.LENGTH_LONG, true).show();
            }
        });
        btnBorrarFirmaREPORTO.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cuadroFirmaREPORTO.clear();
            }
        });

        btnGuardarFirmaAUTORIZO.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toasty.success(getApplicationContext(), "Firma añadida!", Toast.LENGTH_LONG, true).show();
            }
        });
        btnBorrarFirmaAUTORIZO.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cuadrofirmaAUTORIZO.clear();
            }
        });
        btnCambiarFecha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog recogerFecha = new DatePickerDialog(ReportarFalla.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        //Esta variable lo que realiza es aumentar en uno el mes ya que comienza desde 0 = enero
                        final int mesActual = month + 1;
                        //Formateo el día obtenido: antepone el 0 si son menores de 10
                        String diaFormateado = (dayOfMonth < 10) ? CERO + String.valueOf(dayOfMonth) : String.valueOf(dayOfMonth);
                        //Formateo el mes obtenido: antepone el 0 si son menores de 10
                        String mesFormateado = (mesActual < 10) ? CERO + String.valueOf(mesActual) : String.valueOf(mesActual);
                        //Muestro la fecha con el formato deseado
                        cajaFecha.setText(diaFormateado + BARRA + mesFormateado + BARRA + year);

                    }
                    //Estos valores deben ir en ese orden, de lo contrario no mostrara la fecha actual
                }, anio, mes, dia);
                recogerFecha.show();

            }
        });

        btnCambiarHora.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar mcurrenTime = Calendar.getInstance();
                hora = mcurrenTime.get(Calendar.HOUR_OF_DAY);
                minuto = mcurrenTime.get(Calendar.MINUTE);

                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(ReportarFalla.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int horaSeleccionada, int minutosSeleccionados) {
                        cajaHora.setText(horaSeleccionada + ":" + minutosSeleccionados);
                        //Formateo el hora obtenido: antepone el 0 si son menores de 10
                        String horaFormateada = (hora < 10) ? "0" + hora : String.valueOf(horaSeleccionada);
                        //Formateo el minuto obtenido: antepone el 0 si son menores de 10
                        String minutoFormateado = (minuto < 10) ? "0" + minuto : String.valueOf(minutosSeleccionados);
                        //Obtengo el valor a.m. o p.m., dependiendo de la selección del tvIngresoUsuarios
                        String AM_PM;
                        if (hora < 12) {
                            AM_PM = "AM";
                        } else {
                            AM_PM = "PM";
                        }
                        //Muestro la hora con el formato deseado
                        cajaHora.setText(horaFormateada + ":" + minutoFormateado);
                    }

                }, hora, minuto, false);
                mTimePicker.setTitle("Hora");
                mTimePicker.show();
            }
        });

        btnEnviarReporte.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toasty.success(getApplicationContext(), "El reporte de fallo se ha enviado!", Toast.LENGTH_LONG, true).show();
                cuadroFirmaREPORTO.clear();
                cuadrofirmaAUTORIZO.clear();
            }
        });
    }

    private void initElements() {
        tvNombreOperador = findViewById(R.id.tvNombreOperador);
        tvNombreOperador.setText(usuario.getNombreCompleto());
        cuadroFirmaREPORTO = findViewById(R.id.firmaRERPORTO);
        cuadrofirmaAUTORIZO = findViewById(R.id.firmaAUTORIZO);
        btnGuardarFirmaREPORTO = findViewById(R.id.guardarFirmaRERPORTO);
        btnBorrarFirmaREPORTO = findViewById(R.id.limpiarFirmaRERPORTO);
        btnGuardarFirmaAUTORIZO = findViewById(R.id.guardarFirmaAUTORIZO);
        btnBorrarFirmaAUTORIZO = findViewById(R.id.limpiarFirmaAUTORIZO);
        btnCambiarFecha = findViewById(R.id.btnCambiarFecha);
        btnCambiarHora = findViewById(R.id.btnCambiarHora);
        btnEnviarReporte = findViewById(R.id.btnEnviarReporte);
        cajaHora = findViewById(R.id.cajaHora);
        cajaFecha = findViewById(R.id.cajaFecha);
        cajaHora.setEnabled(false);
        cajaFecha.setEnabled(false);
        cajaFecha.setText((dia < 10 ? "0" + dia : dia) + BARRA + (mes < 10 ? "0" + mes : mes) + BARRA + anio);
        cajaHora.setText((hora < 10 ? "0" + hora : hora) + ":" + (minuto < 10 ? "0" + minuto : minuto));
    }

    @Override
    public boolean onSupportNavigateUp() {
        this.finish();
        onBackPressed();
        return false;
    }
}
