package com.bringsolutions.monitoreo.operaciones;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.bringsolutions.monitoreo.R;
import com.bringsolutions.monitoreo.adaptadores.UsuariosAdaptador;
import com.bringsolutions.monitoreo.objetos.Constantes;
import com.bringsolutions.monitoreo.objetos.Usuario;

import java.util.ArrayList;
import java.util.List;

public class GestionUsuarios extends AppCompatActivity {
    private List<Usuario> listaUsuarios;
    private UsuariosAdaptador usuariosAdaptador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gestion_usuarios);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final RecyclerView recyclerView = findViewById(R.id.recyclerUsuarios);
        recyclerView.setLayoutManager(new LinearLayoutManager(GestionUsuarios.this));

        recyclerView.setHasFixedSize(true);
        listaUsuarios = new ArrayList<>();
        listaUsuarios.add(Constantes.user_jefe_planta);
        listaUsuarios.add(Constantes.user_gerente);
        listaUsuarios.add(Constantes.user_opeador_uno);
        listaUsuarios.add(Constantes.user_operador_dos);
        listaUsuarios.add(Constantes.user_operador_tres);

        usuariosAdaptador = new UsuariosAdaptador(listaUsuarios, GestionUsuarios.this);
        recyclerView.setAdapter(usuariosAdaptador);
    }

    @Override
    public boolean onSupportNavigateUp() {
        this.finish();
        onBackPressed();
        return false;
    }
}
