package com.bringsolutions.monitoreo.operaciones;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.bringsolutions.monitoreo.R;
import com.bringsolutions.monitoreo.adaptadores.ReportesAdaptador;
import com.bringsolutions.monitoreo.adaptadores.UsuariosAdaptador;
import com.bringsolutions.monitoreo.objetos.Constantes;
import com.bringsolutions.monitoreo.objetos.Reporte;

import java.util.ArrayList;
import java.util.List;

public class ConsultarReportes extends AppCompatActivity {
    private List<Reporte> listaReportes;
    private ReportesAdaptador reportesAdaptador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consultar_reportes);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final RecyclerView recyclerView = findViewById(R.id.recyclerReportes);
        recyclerView.setLayoutManager(new LinearLayoutManager(ConsultarReportes.this));

        recyclerView.setHasFixedSize(true);
        listaReportes = new ArrayList<>();
        listaReportes.add(Constantes.reporte1);
        listaReportes.add(Constantes.reporte2);
        listaReportes.add(Constantes.reporte3);
        listaReportes.add(Constantes.reporte4);
        listaReportes.add(Constantes.reporte5);

        reportesAdaptador = new ReportesAdaptador(listaReportes, ConsultarReportes.this);
        recyclerView.setAdapter(reportesAdaptador);

    }

    @Override
    public boolean onSupportNavigateUp() {
        this.finish();
        onBackPressed();
        return false;
    }
}
