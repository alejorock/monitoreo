package com.bringsolutions.monitoreo.operaciones;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import com.bringsolutions.monitoreo.R;
import com.bringsolutions.monitoreo.objetos.Reporte;


public class VisualizarReporte extends AppCompatActivity {
    private Intent i;
    private Reporte reporte;

    private TextView tvNombreReporto, tvCargoReporto, tvFecha, tvHora;
    private TextView tvCircuito, tvTipoEquipo, tvPresion, tvNivelCisternaUno, tvNivelCisternaDos;
    private TextView tvObservaciones;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visualizar_reporte);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        i = getIntent();
        reporte = (Reporte) i.getSerializableExtra("reporte");
        initElements();


    }

    private void initElements() {
        tvNombreReporto = findViewById(R.id.tvNombreReporto);
        tvCargoReporto = findViewById(R.id.tvCargoReporto);
        tvFecha = findViewById(R.id.tvFechaReporteVisualizar);
        tvHora = findViewById(R.id.tvHoraReporteVisualizar);
        tvCircuito = findViewById(R.id.tvCicuitoReporte);
        tvTipoEquipo = findViewById(R.id.tvTipoEquipo);
        tvPresion = findViewById(R.id.tvPresion);
        tvNivelCisternaUno = findViewById(R.id.tvNivelCisternaUno);
        tvNivelCisternaDos = findViewById(R.id.tvNivelCisternaDos);
        tvObservaciones = findViewById(R.id.tvObservaciones);


        tvNombreReporto.setText(reporte.getReporto().getNombreCompleto());
        tvCargoReporto.setText(reporte.getReporto().getCargo());
        tvFecha.setText(reporte.getFecha());
        tvHora.setText(reporte.getHora());
        tvCircuito.setText(reporte.getCircuito());
        tvTipoEquipo.setText(reporte.getTipoEquipo());
        tvPresion.setText(reporte.getPresion());
        tvNivelCisternaUno.setText(reporte.getNivelCisternaUno());
        tvNivelCisternaDos.setText(reporte.getNivelCisternaDos());
        tvObservaciones.setText(reporte.getObservaciones());

    }

    @Override
    public boolean onSupportNavigateUp() {
        this.finish();
        onBackPressed();
        return false;
    }
}
