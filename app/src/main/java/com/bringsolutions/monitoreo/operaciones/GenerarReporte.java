package com.bringsolutions.monitoreo.operaciones;

import androidx.appcompat.app.AppCompatActivity;

import android.app.TimePickerDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;

import com.bringsolutions.monitoreo.R;

import java.util.Calendar;

public class GenerarReporte extends AppCompatActivity {
    ImageView btnHora;
    TextView tvHoraReporte;
    Calendar calendario = Calendar.getInstance();
    int hora = calendario.get(Calendar.HOUR_OF_DAY);
    int minuto = calendario.get(Calendar.MINUTE);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_generar_reporte);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);



        inicializarElementos();
        clicks();

    }

    private void clicks() {
        btnHora.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar mcurrenTime = Calendar.getInstance();
                hora = mcurrenTime.get(Calendar.HOUR_OF_DAY);
                minuto = mcurrenTime.get(Calendar.MINUTE);

                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(GenerarReporte.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int horaSeleccionada, int minutosSeleccionados) {
                        tvHoraReporte.setText(horaSeleccionada + ":" + minutosSeleccionados);
                        //Formateo el hora obtenido: antepone el 0 si son menores de 10
                        String horaFormateada = (hora < 10) ? "0" + hora : String.valueOf(horaSeleccionada);
                        //Formateo el minuto obtenido: antepone el 0 si son menores de 10
                        String minutoFormateado = (minuto < 10) ? "0" + minuto : String.valueOf(minutosSeleccionados);
                        //Obtengo el valor a.m. o p.m., dependiendo de la selección del tvIngresoUsuarios
                        String AM_PM;
                        if (hora < 12) {
                            AM_PM = "AM";
                        } else {
                            AM_PM = "PM";
                        }
                        //Muestro la hora con el formato deseado
                        tvHoraReporte.setText(horaFormateada + ":" + minutoFormateado + " " + AM_PM);
                    }

                }, hora, minuto, true);
                mTimePicker.setTitle("Hora");
                mTimePicker.show();
            }
        });
    }

    private void inicializarElementos() {
        btnHora = findViewById(R.id.icono_hora);
        tvHoraReporte = findViewById(R.id.tvHoraReporte);

    }

    @Override
    public boolean onSupportNavigateUp() {
        this.finish();
        onBackPressed();
        return false;
    }
}
