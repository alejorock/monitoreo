package com.bringsolutions.monitoreo.operaciones;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.bringsolutions.monitoreo.R;

public class SolicitarReparacion extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_solicitar_reparacion);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        this.finish();
        onBackPressed();
        return false;
    }
}
