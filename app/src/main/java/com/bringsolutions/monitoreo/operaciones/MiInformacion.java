package com.bringsolutions.monitoreo.operaciones;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import com.bringsolutions.monitoreo.R;
import com.bringsolutions.monitoreo.objetos.Usuario;

public class MiInformacion extends AppCompatActivity {
    private Intent i;
    private Usuario usuario;
    private TextView tvNombrePrincipal;
    private TextView tvNombreCompleto;
    private TextView tvTelefono;
    private TextView tvCargo;
    private TextView tvUsuario;
    private TextView tvEdad;
    private TextView tvAntiguedad;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mi_informacion);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        i = getIntent();
        usuario = (Usuario) i.getSerializableExtra("usuario");
        initElements();

    }

    private void initElements() {
        tvNombreCompleto = findViewById(R.id.tvNombreCompleto);
        tvTelefono = findViewById(R.id.tvTelefono);
        tvCargo = findViewById(R.id.tvCargo);
        tvUsuario = findViewById(R.id.tvUsuario);
        tvEdad = findViewById(R.id.tvEdad);
        tvAntiguedad = findViewById(R.id.tvAntiguedad);

        tvNombrePrincipal = findViewById(R.id.tvNombrePrincipal);
        tvNombreCompleto.setText(usuario.getNombreCompleto());
        tvTelefono.setText(usuario.getTelefono());
        tvCargo.setText(usuario.getCargo());
        tvUsuario.setText(usuario.getUsuario());
        tvNombrePrincipal.setText(usuario.getNombre() + " " + usuario.getApellidoPaterno());
        tvEdad.setText(usuario.getEdad());
        tvAntiguedad.setText(usuario.getAntiguedad());
    }

    @Override
    public boolean onSupportNavigateUp() {
        this.finish();
        onBackPressed();
        return false;
    }
}
