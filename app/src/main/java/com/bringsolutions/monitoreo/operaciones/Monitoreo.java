package com.bringsolutions.monitoreo.operaciones;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bringsolutions.monitoreo.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Calendar;

public class Monitoreo extends AppCompatActivity {
    TextView texto;
    Spinner spnCircuitos;
    TextView tvHora;
    TextView tvFecha;

    //Variables para obtener la fecha
    private Calendar calendario = Calendar.getInstance();
    int hora = calendario.get(Calendar.HOUR_OF_DAY);
    int minuto = calendario.get(Calendar.MINUTE);

    final int mes = calendario.get(Calendar.MONTH) + 1;
    final int dia = calendario.get(Calendar.DAY_OF_MONTH);
    final int anio = calendario.get(Calendar.YEAR);
    private static final String CERO = "0";
    private static final String BARRA = "/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_monitoreo);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        spnCircuitos = findViewById(R.id.spnCisternaMonitoreo);
        tvHora = findViewById(R.id.tvHoraMonitoreo);
        tvFecha = findViewById(R.id.tvFechaMonitoreo);
        texto = findViewById(R.id.textoCambia);
        tvFecha.setText((dia < 10 ? "0" + dia : dia) + BARRA + (mes < 10 ? "0" + mes : mes) + BARRA + anio);
        tvHora.setText((hora < 10 ? "0" + hora : hora) + ":" + (minuto < 10 ? "0" + minuto : minuto));
        accionesSpinner();

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("distancia");


        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                int t = dataSnapshot.getValue(Integer.class);
                //  texto.setText(String.valueOf(t));
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void accionesSpinner() {
        int valorRandom = (int) ((Math.random() * 50) + 1);
        texto.setText(String.valueOf(valorRandom));

        spnCircuitos.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                     Calendar calendario = Calendar.getInstance();
                    int hora = calendario.get(Calendar.HOUR_OF_DAY);
                    int minuto = calendario.get(Calendar.MINUTE);

                    tvHora.setText((hora < 10 ? "0" + hora : hora) + ":" + (minuto < 10 ? "0" + minuto : minuto));

                    int valorRandom = (int) ((Math.random() * 50) + 1);
                    texto.setText(String.valueOf(valorRandom));

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    @Override
    public boolean onSupportNavigateUp() {
        this.finish();
        onBackPressed();
        return false;
    }
}
