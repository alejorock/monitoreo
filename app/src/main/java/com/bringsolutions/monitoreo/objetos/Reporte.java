package com.bringsolutions.monitoreo.objetos;

import java.io.Serializable;

public class Reporte implements Serializable {
    private Usuario reporto;
    private String circuito;
    private String tipoEquipo;
    private String fecha;
    private String hora;
    private String presion;
    private String nivelCisternaUno;
    private String nivelCisternaDos;
    private String observaciones;

    public Reporte() {

    }

    public Reporte(Usuario reporto, String circuito, String tipoEquipo, String fecha, String hora, String presion, String nivelCisternaUno, String nivelCisternaDos, String observaciones) {
        this.reporto = reporto;
        this.circuito = circuito;
        this.tipoEquipo = tipoEquipo;
        this.fecha = fecha;
        this.hora = hora;
        this.presion = presion;
        this.nivelCisternaUno = nivelCisternaUno;
        this.nivelCisternaDos = nivelCisternaDos;
        this.observaciones = observaciones;
    }

    public Usuario getReporto() {
        return reporto;
    }

    public void setReporto(Usuario reporto) {
        this.reporto = reporto;
    }

    public String getCircuito() {
        return circuito;
    }

    public void setCircuito(String circuito) {
        this.circuito = circuito;
    }

    public String getTipoEquipo() {
        return tipoEquipo;
    }

    public void setTipoEquipo(String tipoEquipo) {
        this.tipoEquipo = tipoEquipo;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public String getPresion() {
        return presion;
    }

    public void setPresion(String presion) {
        this.presion = presion;
    }

    public String getNivelCisternaUno() {
        return nivelCisternaUno;
    }

    public void setNivelCisternaUno(String nivelCisternaUno) {
        this.nivelCisternaUno = nivelCisternaUno;
    }

    public String getNivelCisternaDos() {
        return nivelCisternaDos;
    }

    public void setNivelCisternaDos(String nivelCisternaDos) {
        this.nivelCisternaDos = nivelCisternaDos;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }
}
