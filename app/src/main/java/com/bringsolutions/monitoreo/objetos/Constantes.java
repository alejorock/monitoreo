package com.bringsolutions.monitoreo.objetos;

public class Constantes {
    //1 = Gerente
    //2 = Operador
    //3 = Jefe de Planta
    //4 = Jefe del departamento de reparación de equipo
    public static Usuario user_gerente = new Usuario("ENRIQUE", "MORENO", "ARÉVALO", "4", "enrique_gerente", "temporal", "993000000", "Gerente", "54", "5");
    public static Usuario user_opeador_uno = new Usuario("CONCEPCIÓN", "PÉREZ", "SÁNCHEZ", "2", "concepcion_operador", "temporal", "993000000", "Operador", "65", "10");
    public static Usuario user_operador_dos = new Usuario("ANTONIO", "LEÓN", "LEÓN", "2", "antonio_operador", "temporal", "9934456273", "Operador", "52", "3");
    public static Usuario user_operador_tres = new Usuario("JESÚS ANTONIO", "SÁNCHEZ", "DÍAZ", "2", "jesus_operador", "temporal", "993000000", "Operador", "46", "3");
    public static Usuario user_jefe_planta = new Usuario("JOSÚE", "MÉNDEZ", "HERNÁNDEZ", "4", "josue_jefe", "temporal", "993000000", "Jefe de Planta", "54", "6");

    public static Reporte reporte1 = new Reporte(Constantes.user_opeador_uno, "CENTRO", "125 LPS", "21/02/2020", "14:22", "1.9 KG/CM²", "50 m³", "100 m³", "Sin observaciones.");
    public static Reporte reporte2 = new Reporte(Constantes.user_operador_dos, "TABASCO 2000", "250 LPS", "19/01/2020", "20:25", "1.3 KG/CM²", "70 m³", "90 m³", "Reporte hecho en labor nocturna.");
    public static Reporte reporte3 = new Reporte(Constantes.user_opeador_uno, "PERIFÉRICO", "125 LPS", "15/01/2020", "10:15", "2.2 KG/CM²", "90 m³", "80 m³", "Se tomaron las medidas correspondientes.");
    public static Reporte reporte4 = new Reporte(Constantes.user_operador_tres, "CENTRO", "250 LPS", "14/01/2020", "12:11", "4.3 KG/CM²", "25 m³", "50 m³", "Reporte realizado.");
    public static Reporte reporte5 = new Reporte(Constantes.user_operador_dos, "CENTRO", "125 LPS", "10/01/2020", "16:55", "5 KG/CM²", "50 m³", "100 m³", "En espera de verificación.");

}
