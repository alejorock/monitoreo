package com.bringsolutions.monitoreo.objetos;

import java.io.Serializable;

public class Usuario implements Serializable {
    String nombre;
    String apellidoPaterno;
    String apellidoMaterno;
    String tipo;
    String usuario;
    String password;
    String telefono;
    String cargo;
    String edad;
    String antiguedad;

    private Usuario() {
    }

    public Usuario(String nombre, String apellidoPaterno, String apellidoMaterno, String tipo, String usuario, String password, String telefono, String cargo, String edad, String antiguedad) {
        this.nombre = nombre;
        this.apellidoPaterno = apellidoPaterno;
        this.apellidoMaterno = apellidoMaterno;
        this.tipo = tipo;
        this.usuario = usuario;
        this.password = password;
        this.telefono = telefono;
        this.cargo = cargo;
        this.edad = edad;
        this.antiguedad = antiguedad;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public String getEdad() {
        return edad;
    }

    public void setEdad(String edad) {
        this.edad = edad;
    }

    public String getAntiguedad() {
        return antiguedad;
    }

    public void setAntiguedad(String antiguedad) {
        this.antiguedad = antiguedad;
    }

    public String getNombreCompleto() {
        return getNombre() + " " + getApellidoPaterno() + " " + getApellidoMaterno();
    }
}
