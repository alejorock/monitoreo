package com.bringsolutions.monitoreo.adaptadores;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bringsolutions.monitoreo.R;
import com.bringsolutions.monitoreo.objetos.Reporte;
import com.bringsolutions.monitoreo.operaciones.VisualizarReporte;

import java.util.List;

public class ReportesAdaptador extends RecyclerView.Adapter<ReportesAdaptador.ViewHolder> {
    //clase viewholder para enlazar componesntes con la vista de los mensajes
    public static class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tvReporto, tvCircuito, tvConsecutivo;
        private CardView tarjetaReporte;

        public ViewHolder(View itemView) {
            super(itemView);
            //enlanzando elementos
            tvReporto = itemView.findViewById(R.id.tvReporto);
            tvCircuito = itemView.findViewById(R.id.tvCircuito);
            tvConsecutivo = itemView.findViewById(R.id.tvNumConsecutivoReporte);
            tarjetaReporte = itemView.findViewById(R.id.tarjetaReporte);
        }


    }

    public List<Reporte> reporteList;
    public Context context;

    public ReportesAdaptador(List<Reporte> reporteList, Context context) {
        this.reporteList = reporteList;
        this.context = context;
    }

    @NonNull
    @Override
    public ReportesAdaptador.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.tarjeta_item_reporte, viewGroup, false);

        return new ReportesAdaptador.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ReportesAdaptador.ViewHolder viewHolder, final int i) {
        viewHolder.tvConsecutivo.setText("#" + (i + 1));
        viewHolder.tvCircuito.setText(reporteList.get(i).getCircuito());
        viewHolder.tvReporto.setText(reporteList.get(i).getReporto().getNombreCompleto() + "\t" + reporteList.get(i).getFecha() + "\t" + reporteList.get(i).getHora());
        viewHolder.tarjetaReporte.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in;
                in = new Intent(context, VisualizarReporte.class);
                in.putExtra("reporte", reporteList.get(i));
                context.startActivity(in);
            }
        });

    }

    @Override
    public int getItemCount() {
        return reporteList.size();
    }


}
