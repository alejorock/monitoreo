package com.bringsolutions.monitoreo.adaptadores;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bringsolutions.monitoreo.R;
import com.bringsolutions.monitoreo.objetos.Usuario;
import com.bringsolutions.monitoreo.operaciones.MiInformacion;

import java.util.List;

public class UsuariosAdaptador extends RecyclerView.Adapter<UsuariosAdaptador.ViewHolder> {
    //clase viewholder para enlazar componesntes con la vista de los mensajes
    public static class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tvConsecutivo, tvCargo, tvNombre;
        private CardView tarjetaUsuario;

        public ViewHolder(View itemView) {
            super(itemView);
            //enlanzando elementos
            tvConsecutivo = itemView.findViewById(R.id.tvNumConsecutivoTrabajador);
            tvCargo = itemView.findViewById(R.id.tvCargoUsuario);
            tvNombre = itemView.findViewById(R.id.tvNombreTrabajador);
            tarjetaUsuario = itemView.findViewById(R.id.tarjetaUsuario);
        }


    }

    public List<Usuario> usuarioList;
    public Context context;

    public UsuariosAdaptador(List<Usuario> usuarioList, Context context) {
        this.usuarioList = usuarioList;
        this.context = context;
    }

    @NonNull
    @Override
    public UsuariosAdaptador.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.tarjeta_item_usuario, viewGroup, false);

        return new UsuariosAdaptador.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final UsuariosAdaptador.ViewHolder viewHolder, final int i) {
        viewHolder.tvConsecutivo.setText("#" + (i + 1));
        viewHolder.tvCargo.setText(usuarioList.get(i).getCargo());
        viewHolder.tvNombre.setText(usuarioList.get(i).getNombreCompleto());

        viewHolder.tarjetaUsuario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in;
                in = new Intent(context, MiInformacion.class);
                in.putExtra("usuario", usuarioList.get(i));
                context.startActivity(in);
            }
        });

    }

    @Override
    public int getItemCount() {
        return usuarioList.size();
    }


}
