package com.bringsolutions.monitoreo.actores;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.bringsolutions.monitoreo.Login;
import com.bringsolutions.monitoreo.R;
import com.bringsolutions.monitoreo.objetos.Usuario;
import com.bringsolutions.monitoreo.operaciones.MiInformacion;
import com.bringsolutions.monitoreo.operaciones.Monitoreo;
import com.bringsolutions.monitoreo.operaciones.ReportarFalla;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class Operador extends AppCompatActivity {
    private CardView btnMonitoreo, btnReportarFalla, btnMiInformacion;
    private Intent i;
    private Usuario usuario;
    private FloatingActionButton btnCerrarSesion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_operador);
        i = getIntent();
        usuario = (Usuario) i.getSerializableExtra("usuario");
        getSupportActionBar().setTitle("Operador: " + usuario.getNombre());
        initElements();
        clicks();

    }

    private void initElements() {
        btnMonitoreo = findViewById(R.id.btnMonitoreo);
        btnReportarFalla = findViewById(R.id.btnReportarFalla);
        btnMiInformacion = findViewById(R.id.btnInformacionUsuario);
        btnCerrarSesion = findViewById(R.id.btnCerrarSesion);
    }

    private void clicks() {
        btnMonitoreo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Operador.this, Monitoreo.class));
            }
        });

        btnReportarFalla.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                i = new Intent(Operador.this, ReportarFalla.class);
                i.putExtra("usuario", usuario);
                startActivity(i);
            }
        });

        btnMiInformacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                i = new Intent(Operador.this, MiInformacion.class);
                i.putExtra("usuario", usuario);
                startActivity(i);
            }
        });
        btnCerrarSesion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(Operador.this);
                builder.setTitle("CERRAR SESIÓN");
                builder.setMessage("¿Desea cerrar sesión?");

                builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Operador.this.finish();
                        i = new Intent(Operador.this, Login.class);
                        startActivity(i);
                    }
                });
                builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });
    }
}
