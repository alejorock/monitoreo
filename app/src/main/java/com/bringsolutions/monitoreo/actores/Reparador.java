package com.bringsolutions.monitoreo.actores;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.bringsolutions.monitoreo.Login;
import com.bringsolutions.monitoreo.R;
import com.bringsolutions.monitoreo.objetos.Usuario;
import com.bringsolutions.monitoreo.operaciones.MiInformacion;
import com.bringsolutions.monitoreo.operaciones.ReportarFalla;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class Reparador extends AppCompatActivity {
    private Intent i;
    private CardView btnSolcitarReparacion, btnMiInformacion;
    private Usuario usuario;
    private FloatingActionButton btnCerrarSesion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reparador);
        i = getIntent();
        usuario = (Usuario) i.getSerializableExtra("usuario");
        getSupportActionBar().setTitle("Jefe de Reparación: " + usuario.getNombre());
        initElements();
        clicks();

    }

    private void clicks() {
        btnSolcitarReparacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                i = new Intent(Reparador.this, ReportarFalla.class);
                i.putExtra("usuario", usuario);
                startActivity(i);
            }
        });
        btnMiInformacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                i = new Intent(Reparador.this, MiInformacion.class);
                i.putExtra("usuario", usuario);
                startActivity(i);
            }
        });
        btnCerrarSesion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(Reparador.this);
                builder.setTitle("CERRAR SESIÓN");
                builder.setMessage("¿Desea cerrar sesión?");

                builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Reparador.this.finish();
                        i = new Intent(Reparador.this, Login.class);
                        startActivity(i);
                    }
                });
                builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });
    }

    private void initElements() {
        btnSolcitarReparacion = findViewById(R.id.btnSolicitarReparacion);
        btnMiInformacion = findViewById(R.id.btnInformacionUsuario);
        btnCerrarSesion = findViewById(R.id.btnCerrarSesion);
    }
}
