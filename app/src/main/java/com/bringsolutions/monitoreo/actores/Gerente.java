package com.bringsolutions.monitoreo.actores;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.bringsolutions.monitoreo.Login;
import com.bringsolutions.monitoreo.R;
import com.bringsolutions.monitoreo.objetos.Usuario;
import com.bringsolutions.monitoreo.operaciones.ConsultarGraficas;
import com.bringsolutions.monitoreo.operaciones.ConsultarReportes;
import com.bringsolutions.monitoreo.operaciones.MiInformacion;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class Gerente extends AppCompatActivity {
    private CardView btnConsultarReportes, btnConsultarGraficas, btnInformacionUsuario;
    private Intent i;
    private Usuario usuario;
    private FloatingActionButton btnCerrarSesion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gerente);
        i = getIntent();
        usuario = (Usuario) i.getSerializableExtra("usuario");
        getSupportActionBar().setTitle("Gerente: " + usuario.getNombre());

        initElements();
        clicks();
    }

    private void initElements() {
        btnConsultarReportes = findViewById(R.id.btnConsultarReportes);
        btnConsultarGraficas = findViewById(R.id.btnConsultarGraficas);
        btnInformacionUsuario = findViewById(R.id.btnInformacionUsuario);
        btnCerrarSesion = findViewById(R.id.btnCerrarSesion);
    }

    private void clicks() {
        btnConsultarReportes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Gerente.this, ConsultarReportes.class));
            }
        });

        btnConsultarGraficas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Gerente.this, ConsultarGraficas.class));
            }
        });
        btnInformacionUsuario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                i = new Intent(Gerente.this, MiInformacion.class);
                i.putExtra("usuario", usuario);
                startActivity(i);
            }
        });
        btnCerrarSesion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(Gerente.this);
                builder.setTitle("CERRAR SESIÓN");
                builder.setMessage("¿Desea cerrar sesión?");

                builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Gerente.this.finish();
                        i = new Intent(Gerente.this, Login.class);
                        startActivity(i);
                    }
                });
                builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });
    }


}
