package com.bringsolutions.monitoreo.actores;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.bringsolutions.monitoreo.Login;
import com.bringsolutions.monitoreo.R;
import com.bringsolutions.monitoreo.objetos.Usuario;
import com.bringsolutions.monitoreo.operaciones.ConsultarGraficas;
import com.bringsolutions.monitoreo.operaciones.ConsultarReportes;
import com.bringsolutions.monitoreo.operaciones.GenerarGraficas;
import com.bringsolutions.monitoreo.operaciones.GenerarReporte;
import com.bringsolutions.monitoreo.operaciones.GestionUsuarios;
import com.bringsolutions.monitoreo.operaciones.MiInformacion;
import com.bringsolutions.monitoreo.operaciones.Monitoreo;
import com.bringsolutions.monitoreo.operaciones.ReportarFalla;
import com.bringsolutions.monitoreo.operaciones.SolicitarReparacion;
import com.google.android.material.floatingactionbutton.FloatingActionButton;


public class JefePlanta extends AppCompatActivity {
    private CardView btnGenerarReportes, btnGenerarGraficas, btnMonitoreo, btnReportarFalla, btnConsultarReportes, btnConsultarGraficas, btnSolicitarReparacion, btnGestionUsuarios, btnInformacionUsuario;
    private Intent i;
    private Usuario usuario;
    private FloatingActionButton btnCerrarSesion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jefe_planta);

        i = getIntent();
        usuario = (Usuario) i.getSerializableExtra("usuario");
        getSupportActionBar().setTitle("Jefe de Planta: " + usuario.getNombre());

        initElements();
        clicks();
    }

    private void clicks() {
        btnGenerarReportes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(JefePlanta.this, GenerarReporte.class));
            }
        });

        btnGenerarGraficas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(JefePlanta.this, GenerarGraficas.class));
            }
        });

        btnMonitoreo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(JefePlanta.this, Monitoreo.class));
            }
        });

        btnReportarFalla.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                i = new Intent(JefePlanta.this, ReportarFalla.class);
                i.putExtra("usuario", usuario);
                startActivity(i);
            }
        });

        btnConsultarReportes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(JefePlanta.this, ConsultarReportes.class));
            }
        });

        btnConsultarReportes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(JefePlanta.this, ConsultarReportes.class));
            }
        });

        btnConsultarGraficas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(JefePlanta.this, ConsultarGraficas.class));
            }
        });

        btnSolicitarReparacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(JefePlanta.this, SolicitarReparacion.class));
            }
        });

        btnGestionUsuarios.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(JefePlanta.this, GestionUsuarios.class));
            }
        });

        btnInformacionUsuario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                i = new Intent(JefePlanta.this, MiInformacion.class);
                i.putExtra("usuario", usuario);
                startActivity(i);
            }
        });

        btnCerrarSesion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(JefePlanta.this);
                builder.setTitle("CERRAR SESIÓN");
                builder.setMessage("¿Desea cerrar sesión?");

                builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        JefePlanta.this.finish();
                        i = new Intent(JefePlanta.this, Login.class);
                        startActivity(i);
                    }
                });
                builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });
    }

    private void initElements() {
        btnGenerarReportes = findViewById(R.id.btnGenerarReportes);
        btnGenerarGraficas = findViewById(R.id.btnGenerarGraficas);
        btnMonitoreo = findViewById(R.id.btnMonitoreo);
        btnReportarFalla = findViewById(R.id.btnReportarFalla);
        btnConsultarReportes = findViewById(R.id.btnConsultarReportes);
        btnConsultarGraficas = findViewById(R.id.btnConsultarGraficas);
        btnSolicitarReparacion = findViewById(R.id.btnSolicitarReparacion);
        btnGestionUsuarios = findViewById(R.id.btnGestionUsuarios);
        btnInformacionUsuario = findViewById(R.id.btnInformacionUsuario);
        btnCerrarSesion = findViewById(R.id.btnCerrarSesion);
    }

}
